# IMbeeUI SDK 
SDK de Imbee para realizar la conexión de chatbots en la plataforma Android.


## Funcionalidades
- Plug and Play
- Alto grado de personalización 
- Instalable a través de gradle
- Compatible con Java y Kotlin


## Requirements
- Android 5.0+

## Instalación

Em build.gradle de proyecto:


```java
url "${artifactory_contextUrl}/libs-release"
            credentials {
                username = "${artifactory_user}"
                password = "${artifactory_password}"
            }


```
En gradle.properties

```java
artifactory_contextUrl=http://maven.developers.imbee.es
artifactory_user=imbee
artifactory_password=publicsdk
```

En build.gradle de app:

```java
api(group: 'com.imbee.ui', name: 'imbeeuilib', version: '2.0.1', ext: 'aar') {
        transitive = true
    }
```


# Uso del SDK

Lo primero que hay que hacer es conectar la aplicación los servidores de Imbee a través de `ImbeeCore`
```java

ImbeeEnvironmentModel environmentModel = new ImbeeEnvironmentModel(
                    WS_URL, //La proporcinoará IMbee
                    XMPP_URL, //La proporcinoará IMbee
                    APIKEY, //La proporcinoará IMbee
                    IDSHA_BOT, //La proporcinoará IMbee
                    true); //booleano para activar/desactivar logs de conexión

ImbeeUserDeviceModel userDeviceModel = new ImbeeUserDeviceModel(userId, "hardwareId", ImbeeDeviceInfo.PLATFORM, ImbeeDeviceInfo.getDeviceName(), ImbeeDeviceInfo.getOSVersionCode());
            ImbeeCore.getInstance().init(getApplication(), new ImbeeConfigurationModel(userDeviceModel, environmentModel), new ImbeeCoreInitListener() {

                @Override
                public void onImbeeCoreInitSuccess(String conversationId, boolean previouslyCreated, boolean previouslyRegistered, boolean showOnboarding) {
                    //Core inicializado correctamente, con previouslyRegistered sabemos si el usuario ya existía
                    // con previouslyCreated sabemos si la conversación entre ese usuario y el bot ya existía
                }

                @Override
                public void onImbeeCoreInitError(String errorMessage) {

                }
            });

```

Una vez recibido el evento onImbeeCoreInitSuccess podemos abrir la UI para ver los mensajes, la UI es un fragment que incluiremos en una activity:

```java
private IMbeeChatVoiceFragment chatFragment;

private void setChatFragment() {
        chatFragment = IMbeeChatVoiceFragment.getInstance();
        chatFragment.waitingWelcome = true; // Si esperamos un mensaje de bienvenida
        chatFragment.setSetBotAppParams("Mensaje invisible que se envía al bot para que nos salude o configurar la conversación(idioma, saludo personalizado, etc..."); 

        chatFragment.setCallbacks(new IMbeeChatVoiceFragmentCallbacks() {
            @Override
            public void onButtonClicked(MessageReceivedModel message, ButtonDto buttonContent) {
                //Aquí recogemos cuando se pulsa un botón de un mensaje con botones 
                Log.d(TAG, "onButtonClicked");
                if (buttonContent.buttonType.equals("url")) {
                    //Si es tipo url el implementador decide qué hace con esa url, en el ejemplo abrimos el navegador por defecto
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(buttonContent.payload));
                    try {
                        startActivity(browserIntent);
                    } catch (ActivityNotFoundException exception) {
                        Log.e(TAG, exception.getMessage());
                    }
                }
            }

            @Override
            public void onLinkClicked(String url) {
                //Mensajes tipo link, ya no se suelen utilizar
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCloseButtonClicked() {
                //Para saber cuando se ha pulsado la cruz para salir del fragment
            }

            @Override
            public void onFragmentStarted() {
                //Evento que nos indica que el fragment está inicializado
            }

            @Override
            public void onMicroButtonClicked() {
                Log.d(TAG, "onMicroButtonClicked");
            }

            @Override
            public void onSentMessage() {
                Log.d(TAG, "onSentMessage");
            }

            @Override
            public void onPhraseWakesMicro() {
                Log.d(TAG, "onPhraseWakesMicro");
            }

            @Override
            public void onCustomButtonClicked(ChatbotMessage message, IMbeeCustomButton button) {
                //Mismo funcionamiento que onButtonClicked pero solo se utiliza en campañas personalizadas
                if (button.type.equals("url")) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(button.payload));
                        startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onWelcomeMessageReceived() {
                //Solo se suele necesitar para casos personalizados
            }

        });

        getSupportFragmentManager().beginTransaction()
                .add(R.id.chat_fragment, chatFragment).commit();
    }

```

Para personalizar la apariencia del chat, tenemos diversos parámetros en la clase IMbeeUI que se utilizan como el ejemplo a continuación:

```java
private void setupChat() {
        IMbeeUI.initIMbeeUI(this); //Obligatorio, el resto es opcional, puesto que hay valores por defecto.

        IMbeeUI.setStartVoiceAssistantPhrase("Hola Gina");
        IMbeeUI.voiceChatWelcomeMessageString = "¡Oh! me encanta volver a tenerte por aquí, mira lo nuevo que tengo preparado para ti";
        IMbeeUI.setHideKeyboard(true);
        IMbeeUI.setPrimaryColor(Color.parseColor("#0076d6"));
        IMbeeUI.setQuickReplyButtonBorderColor(Color.parseColor("#0076d6"));
        IMbeeUI.setPressedQuickReplyButtonBorderColor(Color.parseColor("#880076d6"));
        IMbeeUI.setQuickReplyButtonTextColor(Color.parseColor("#000000"));
        IMbeeUI.setPressedQuickReplyButtonTextColor(Color.parseColor("#88000000"));
        IMbeeUI.setQuickReplyButtonBackgroundColor(Color.parseColor("#ffffff"));
        IMbeeUI.setPressedQuickReplyButtonBackgroundColor(Color.parseColor("#ffffff"));

        IMbeeUI.setRightMessageBackgroundColor(Color.parseColor("#0076d6"));
        IMbeeUI.setRightMessageTextColor(Color.parseColor("#ffffff"));
        IMbeeUI.setLeftMessageBackgroundColor(Color.parseColor("#ffffff"));
        IMbeeUI.setLeftMessageTextColor(Color.parseColor("#000000"));

        IMbeeUI.setTypefaceNormal(Typeface.SANS_SERIF);//createFromAsset(getAssets(), "fonts/opensans-regular.ttf"));
        IMbeeUI.setTypefaceBold(Typeface.createFromAsset(getAssets(), "fonts/opensans-bold.ttf"));

        IMbeeUI.setSpeechRecognitionAnimationColor(Color.parseColor("#0076d6"));
        IMbeeUI.setSpeechRecognitionAnimationBackgroundColor(Color.parseColor("#63007eae"));

        IMbeeUI.setSystemMessageColor(Color.parseColor("#000000"));
        IMbeeUI.setSystemMessageBackgroundColor(Color.parseColor("#ededed"));

        IMbeeUI.setHideMessageStates(true);
        IMbeeUI.setHideMessageTime(true);

        float messageRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
        IMbeeUI.setMessageRadius(messageRadius);
        IMbeeUI.setSystemMessageRadius(messageRadius);

        float r = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        float[] leftRadius = {r, r, r, r, r, r, 0, 0};
        float[] rightRadius = {r, r, r, r, 0, 0, r, r};
        IMbeeUI.setLeftMessageRadius(leftRadius);
        IMbeeUI.setRightMessageRadius(rightRadius);

        int p = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
        int p2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        IMbeeUI.setLeftMessageBackgroundPadding(p2, p, p2, p);
        IMbeeUI.setRightMessageBackgroundPadding(p2, p, p2, p);

        IMbeeUI.setSendMessageButtonDrawable(ContextCompat.getDrawable(this, R.drawable.send_cx));
        IMbeeUI.setSpeechRecognizerButtonDrawable(ContextCompat.getDrawable(this, R.drawable.mic_cx));
        IMbeeUI.setSpeechRecognizerCloseDrawable(ContextCompat.getDrawable(this, R.drawable.ok));

        IMbeeUI.setVoiceRecognizingLabel("Habla, te escucho...");
        IMbeeUI.setVoiceRecognizingButton(ContextCompat.getDrawable(this, R.drawable.ic_micro));

        IMbeeUI.voiceChatKeyboardButtonDrawable = ContextCompat.getDrawable(this, R.drawable.chatbot_keyboard);
        IMbeeUI.voiceChatMicrophoneButtonDrawable = ContextCompat.getDrawable(this,R.drawable.chatbot_voice);

        Set<String> a = new HashSet<>();
        a.add("female");

        IMbeeUI.setSpeechToTextVoice(new Voice("es-es-x-ana#female_1-local", new Locale("es"
                , "ES"), 400, 200, true, a));
        IMbeeUI.setTextToSpeechRate(1.2f);

        int[] voiceAnimationColors = {
                Color.parseColor("#99c2ff"),
                Color.parseColor("#4d94ff"),
                Color.parseColor("#0066ff"),
                Color.parseColor("#99c2ff"),
                Color.parseColor("#0066ff")
        };

        IMbeeUI.setVoiceAnimationColors(voiceAnimationColors);

        IMbeeUI.permissionRecordAudioTitle = "Aviso";
        IMbeeUI.permissionRecordAudioMessage = "Necesitamos acceder al micrófono para utilizarlo " +
                "en la aplicación";
        IMbeeUI.permissionRecordAudioLeftButton = "No";
        IMbeeUI.permissionRecordAudioRightButton = "Configuración";

        IMbeeUI.setSpeechRecognizerLanguage("es_ES");
        IMbeeUI.setVoiceTag("voice");

        IMbeeUI.setTypingDotWaveAnimation(true);

        IMbeeUI.voiceChatBackgroundColor = Color.parseColor("#ffffff");

        //Para personalizar los avatares
        IMbeeUI.setMessageProfileCustomization(new IMbeeUI.MessageProfileCustomization() {
            @Override
            public View getView(MessagesListAdapter adapter, int position) {
                RelativeLayout relativeLayout;
                
                ChatbotMessage message = adapter.getItem(position);

                if (message != null && message.type == 0) {
                    //avatar del bot
                    relativeLayout = (RelativeLayout) LayoutInflater.from(getApplicationContext()).inflate(profileImageLeft, null);

                    return relativeLayout;
                }
                else {
                    //avatar del usuario
                    relativeLayout = (RelativeLayout) LayoutInflater.from(getApplicationContext()).inflate(profileImageRight, null);
                    return relativeLayout;
                }
            }
        });
}
```

Para enviar un mensaje desde la activity(en caso de que se tenga algún botón u opción fuera del fragment):

```java
    public void sendMessage(String textToSend, String payload, int flags) {
        MessageSentModel message = new MessageSentModel(textToSend, payload, 0);
        ImbeeCore.sendMessage(message);
    }
```
donde
    - textToSend sería la parte visible del mensaje a enviar
    - payload sería contenido invisible para el usuario, pero que puede requerir el bot en una respuesta
    - flags: XMPPFormat.FLAG_VISIBLE o XMPPFormat.FLAG_INVISIBLE (Según queramos que aparezca en el chat o no)


Una vez finalizado el uso del bot o cuando se quiera finalizar la sesión:

```java
    private void stopCore() {
        ImbeeCore.getInstance().stopCore();
        IMbeeProvider.getInstance().clear();
    }
```

## Transaccionalidad
``` java
    public void onReceivedExternalContentMessage(String externalContentType, String externalData) {
        if (externalContentType.equals("get_authcode"))) {
		// Cuando el mensaje recibido coincide con un external type significa que se requiere de una seguridad extra de autenticación para realizar la tarea. Para el implementado solo hay que comprobar este tipo de mensaje y realizar la obtención de seguridad pertinente
        } else {
            // Es cualquier otro tipo de mensaje.
        }

```

## Contacto
Para cualquier otra duda, podéis contactar a jmolinas@imbee.me
