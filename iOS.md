# ImbeeUISDK 
SDK de Imbee para realizar la conexión de chatbots en la plataforma iOS.


## Funcioalidades
- Plug and Play
- Alto grado de personalización a través de Delegates y Datasources
- Instalable a través de Cocoapods
- Compatible con Swift y ObjectiveC


## Requirements
- iOS 9.0+

## Instalación

### CocoaPods
Puedes usar [CocoaPods](http://cocoapods.org/) para instalar `IMbeeUISDK` añadiéndolo al `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
source 'https://gitlab.com/imbee/ios/specs.git'

platform :ios, '9.0'
target 'app_name' do
  use_frameworks!
  pod 'IMbeeUISDK'

  post_install do |installer|
        installer.pods_project.targets.each do |target|
            target.build_configurations.each do |config|
                config.build_settings['ENABLE_BITCODE'] = 'YES'
                config.build_settings['SWIFT_VERSION'] = '5.0'
            end
        end
    end
end

```

# Uso del SDK

Lo primero que hay que hacer es conectar la aplicación los servidores de Imbee a través de `ImbeeCore`
```swift 
IMbeeCore.registerAndGetConversation(
            withIdShaBot: "SHA DEL BOT",
            userId: "ID DE USUARIO",
            serverAddress: "ENDPOINT SERVER",
            serverXmpp: "ENDPOINT XMPP",
            serverAdmin: "ENDPOINT ADMIN (OPCIONAL)",
            xmppUserDomain: "USUARIO XMPP",
            apiKey: "IMBEE API KEY",
            stanzaV: 0.0, // NÚMERO DE ESTANZA QUE SE USARÁ
            withHistory: false) {
                { [weak self] (conversation, profile, previouslyRegistered, conversation_previously_created, needsToShowOnboarding) in
                    guard let s = self else { return }
                    guard let c = conversation else { return }
                    print("CONEXIÓN CON EL BOT REALIZADA CON ÉXITO")
                    // En este punto se obtiene el conversation 
                }
            }
```

Una vez conectada el `core` ya podemos instanciar y presentar el chatbot. Presentar el chatbot es tan senzillo como seguir los siguientes pasos:
1. Instanciar el `IMBeeUIViewController`
```swift 
lazy var vc: IMBeeUIViewController = IMBeeUIViewController(voiceBankingDataSource: self, voiceBankingDelegate: self)
```
2. Presentar el view controller
```swift 
// Con navigation controller
let nav = UINavigationController(rootViewController: vc)
self.present(nav, animated: true, completion: nil)

// Sin navigation controller
self.present(vc, animated: true, completion: nil)
```

3. Implementar el Delegate para personalizar la UI
```swift
// Hay métodos obligatiorios y opcionales a implementar
@objc
public protocol IMBeeUIViewControllerDelegate: class {
    func actionWasPressed(text: String, type: String, payload: String, webUrl: String?)
	func equalizerDarkColor() -> UIColor
	func equalizerLightColor() -> UIColor
	func typingOnDotsColor() -> UIColor
	func botBubbleBorderColor() -> UIColor
	func botMessageTextColor() -> UIColor
	func botMessageBackgroundColor() -> UIColor
	func userBubbleBorderColor() -> UIColor
	func userMessageTextColor() -> UIColor
	func userMessageBackgroundColor() -> UIColor
	func listeningPlaceholderTextColor() -> UIColor
	func sendButtonImage() -> UIImage
	func microButtonImage() -> UIImage
	func microExpandedButtonImage() -> UIImage
	func quickReplyCornerRadius() -> Int
	func quickReplyBackgroundColor() -> UIColor
	func quickReplyTextColor() -> UIColor
	func quickReplyBorderColor() -> UIColor
	func inputFont() -> UIFont
	func listeningFont() -> UIFont
	func botTextFont() -> UIFont
	func userTextFont() -> UIFont
	func buttonsFont() -> UIFont
	
	func permissionsDeniedTitle() -> String
	func permissionsDeniedSubtitle() -> String
	func permissionsDeniedCancelText() -> String
	func permissionsDeniedSettingsText() -> String
	func permissionsDeniedTitleColor() -> UIColor
	func permissionsDeniedSubtitleColor() -> UIColor
	func permissionsDeniedCancelTextColor() -> UIColor
	func permissionsDeniedSettingsTextColor() -> UIColor
	func permissionsDeniedTitleFont() -> UIFont
	func permissionsDeniedSubtitleFont() -> UIFont
	func permissionsDeniedCancelFont() -> UIFont
	func permissionsDeniedSettingsFont() -> UIFont
	
    @objc optional func stateWasChanged(from: IMBeeStatesManager.State, to: IMBeeStatesManager.State)
    @objc optional func titleStyle(for state: IMBeeStatesManager.State) -> IMBeeUITextStyle
    @objc optional func messageReceived(message: Message)
    @objc optional func keyboardWasPressed()
    @objc optional func voiceWasPressed()
    @objc optional func didAwakeByVoice()
    @objc optional func messageWasSent(message: Message)
	@objc optional func needsToRegisterTrackingEvent(event: TrackingEvent, id: String)
	@objc optional func campaignBorderColor() -> UIColor
	@objc optional func campaignTitleColor() -> UIColor
	@objc optional func campaignSubtitleColor() -> UIColor
	@objc optional func campaignCTATextColor() -> UIColor
	@objc optional func campaignCornerRadius() -> Int
	@objc optional func campaignTitleFont() -> UIFont
	@objc optional func campaignSubtitleFont() -> UIFont
	@objc optional func campaignCTAFont() -> UIFont
}
```

4. Implementar el DataSource para pasar los datos necesarios al SDK
``` swift 
@objc
public protocol IMBeeUIViewControllerDataSource: class {
    func conversation() -> Conversation?
    func botImage() -> UIImage
    func userImage(_ reloadBlock: @escaping (UIImage) -> Void) -> UIImage?
	func language() -> IMBeeLanguage
	func inputBarPlaceholderText() -> String
	func listeningPlaceholderText() -> String
    func botParams() -> String
	func awakeAssistantConfiguration() -> IMBeeAwakeConfiguration
    
	@objc optional func campaign() -> Campaign?
    @objc optional func timeoutSeconds() -> Double
}
```

5. Al presentar el view controller, internamente se llama a los métodos de delegate y datasource para crear y personalizar la ui.

## Ejemplo
```swift 

extension DummyViewController: IMBeeUIViewControllerDelegate {
	func actionWasPressed(text: String, type: String, payload: String, webUrl: String?) {
		print(text, type, payload)
	}
	
	func equalizerDarkColor() -> UIColor {
		return UIColor(hexString: "00E9C5")
	}
	
	func equalizerLightColor() -> UIColor {
		return UIColor(hexString: "7FF4E2")
	}
	
	func typingOnDotsColor() -> UIColor {
		return UIColor(hexString: "7FF4E2")
	}
	
	func microButtonImage() -> UIImage {
		return UIImage(named: "img-user")!
	}
	
	func sendButtonImage() -> UIImage {
		return UIImage(named: "img-bot")!
	}
	
	func botBubbleBorderColor() -> UIColor {
		return UIColor.red
	}
	
	func botMessageTextColor() -> UIColor {
		return UIColor.gray
	}
	
	func userBubbleBorderColor() -> UIColor {
		return UIColor.red
	}
	
	func userMessageTextColor() -> UIColor {
		return UIColor.gray
	}
	
	func listeningPlaceholderTextColor() -> UIColor {
		return UIColor.gray
	}
	
	func botMessageBackgroundColor() -> UIColor {
		return UIColor(white: 0.9, alpha: 1)
	}
	
	func userMessageBackgroundColor() -> UIColor {
		return UIColor(hexString: "00E9C5")
	}
	
	// Use a value < 0 to round as a circle
	func quickReplyCornerRadius() -> Int {
		return -1
	}
	
	func quickReplyBackgroundColor() -> UIColor {
		return UIColor(hexString: "00E9C5")
	}
	
	func quickReplyTextColor() -> UIColor {
		return UIColor.red
	}
	
	func quickReplyBorderColor() -> UIColor {
		return .blue
	}
	
	func microExpandedButtonImage() -> UIImage {
		return UIImage(named: "img-bot")!
	}
	
	func campaignBorderColor() -> UIColor {
		return .green
	}
	
	func campaignTitleColor() -> UIColor {
		return .blue
	}
	
	func campaignSubtitleColor() -> UIColor {
		return .purple
	}
	
	func campaignCTATextColor() -> UIColor {
		return .blue
	}
	
	func campaignCornerRadius() -> Int {
		return 15
	}
	
	func campaignTitleFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .body)
	}
	func campaignSubtitleFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .caption1)
	}
	func campaignCTAFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .footnote)
	}
	
	func inputFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .title2)
	}
	
	func listeningFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .title1)
	}
	
	func botTextFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .title1)
	}
	
	func userTextFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .title3)
	}
	
	func buttonsFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .footnote)
	}
	
	func permissionsDeniedTitle() -> String {
		return "Foo"
	}
	func permissionsDeniedSubtitle() -> String {
		return "Bar"
	}
	func permissionsDeniedCancelText() -> String {
		return "Cancel"
	}
	
	func permissionsDeniedSettingsText() -> String {
		return "Settings"
	}
	
	
	func permissionsDeniedTitleColor() -> UIColor {
		return .black
	}
	
	func permissionsDeniedSubtitleColor() -> UIColor {
		return .black
	}
	
	func permissionsDeniedCancelTextColor() -> UIColor {
		return .black
	}
	
	func permissionsDeniedSettingsTextColor() -> UIColor {
		return .black
	}
	
	func permissionsDeniedTitleFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .footnote)
	}
	
	func permissionsDeniedSubtitleFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .footnote)
	}
	
	func permissionsDeniedCancelFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .footnote)
	}
	
	func permissionsDeniedSettingsFont() -> UIFont {
		return UIFont.preferredFont(forTextStyle: .footnote)
	}
}

extension DummyViewController: IMBeeUIViewControllerDataSource {
	func conversation() -> Conversation? {
		return conv
	}
	
	func botImage() -> UIImage {
		return UIImage(named: "img-user")!
	}
	
	func userImage(_ reloadBlock: @escaping (UIImage) -> Void) -> UIImage? {
		return nil
	}
	
	func language() -> IMBeeLanguage {
		return .spanish
	}

	func botParams() -> String {
		return environment!.appBotPrefix
	}
	
	func awakeAssistantConfiguration() -> IMBeeAwakeConfiguration {
		return IMBeeAwakeConfiguration(
			language: .spanish,
			wakeUpSentence: "Hola Bot",
			levenshteinDelta: 3)
	}
	
	func inputBarPlaceholderText() -> String {
		return "Escribe un mensaje"
	}
	
	func listeningPlaceholderText() -> String {
		return "Te estoy escuchando..."
	}
	
	func campaign() -> Campaign? {
		return Campaign(
			imageUrl: "https://www.40defiebre.com/wp-content/uploads/2015/10/imagenes.png",
			title: "Title",
			subtitle: "Subtitle",
			buttonText: "Button text",
			buttonType: "postback",
			buttonCta: "CTA")
	}
}
```

## Transaccionalidad
La funcionalidad de transaccionalidad permite conseguir *tokens* de seguridad a través del SDK para el implementador. 
```swift 
public func messageReceived(messageIdentifier: String, payload: String?, externalType: String?, externalData: [AnyHashable : Any]?) {
    if externalType == "get_authcode" {
		// Cuando el mensaje recibido coincide con un external type significa que se requiere de una seguridad extra de autenticación para realizar la tarea. Para el implementado solo hay que comprobar este tipo de mensaje y realizar la obtención de seguridad pertinente
    } else {
		// Es cualquier otro tipo de mensaje.
	}
```

## Contacto
Para cualquier otra duda, podéis contactar a jmolinas@imbee.me


